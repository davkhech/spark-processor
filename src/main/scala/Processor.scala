import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object Processor {
  def main(args: Array[String]) {

    val spark = SparkSession
      .builder
      .master("local[*]")
      .appName("Data Processor")
      .getOrCreate()

    val date = if (args.length > 0) args(0) else null
    val input = if (args.length > 0) args(1) else "/Users/demo/Downloads/data_part"
    val output = if (args.length > 0) args(2) else "/Users/demo/Documents/workspace/output_dir"

    val exampleDf = spark.read
      .json(input + "/*")
      .distinct

    val withDate = exampleDf.withColumn("date", from_unixtime(exampleDf.col("ts") / 1000, "yyyy-MM-dd"))

    val finalDf = if (date == null) withDate else withDate.filter(row => date == row.getAs[String]("date"))

    finalDf.write.partitionBy("date").mode("overwrite").parquet(output + "/parquet")
    spark.stop()
  }
}
