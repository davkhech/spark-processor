##
Module to read JSON dataset, remove duplicates, partition data by time and write to parquet and avro

To build run
`sbt clean package`

### Command line arguments
1. Date - process only data for given date
2. Input directory
3. Output directory
